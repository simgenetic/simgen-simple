package com.simgen;

import java.io.FileInputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.ejml.simple.SimpleMatrix;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.simgen.model.Individual;
import com.simgen.model.Population;
import com.simgen.model.SimParameters;

/**
 * GeneticSimulation class used to start the simulator.
 * @author ckunzi
 *
 */
public class GeneticSimulation {
	
	private static Logger logger = Logger.getLogger(GeneticSimulation.class);
    
    SimpleMatrix phenotypeOption = new SimpleMatrix();
    
    //rowvec phen_opt = ones<rowvec>(n_genes);
    double omega = 0.5;

	/**
	 * Reads the config file and transform it to object to launch the simulation
	 * @param args 1st argument should be the name of the json config file.
	 */
	public static void main(String args[]){
		
		Individual individual1 = new Individual(null);
		Individual individual2 = new Individual(null);
		
		Population population = new Population(individual1,individual2);
		
		for(int i=0; i<SimParameters.individualNumber; i++){
			population.generateOffspring();
		}

		logger.info("simulation completed successfully");
		
	}
	

}
