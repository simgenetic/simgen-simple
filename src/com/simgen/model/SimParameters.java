package com.simgen.model;

public class SimParameters {

	public static int geneNumber = 25;                    // number of genes in network
	public static int iterationsForConvergence = 100;		// number of iterations in convergence test
	public static int phenotypeNumber = 25;
	public static int stock_gamete = 10;
	public static int individualNumber = 10;
	public static double epsilon = 0.000000001;
	public static double self_rate = 0;
	public static double backcross_rate = 0.5;
	public static double mutation_rate = 0;
	
	
}
