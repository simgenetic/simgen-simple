package com.simgen.model;

import org.ejml.simple.SimpleMatrix;

public class Individual {

	private Gamete gamete;

	public Individual(Gamete gamete){
		this.gamete = gamete;
	}
	
	public Individual clone(){
		return new Individual(this.gamete.clone());
	}
	
	public double getFitness(){
		return 0.0;
	}
	
	public SimpleMatrix getPhenotype(){
		
		SimpleMatrix simpleMatrix = new SimpleMatrix(SimParameters.geneNumber,SimParameters.geneNumber);
		
		for(int i=0; i<SimParameters.geneNumber; i++)
		{
			for(int j=0; j<SimParameters.geneNumber; j++){
				simpleMatrix.set( i , j , 0.5 ); 
			}
		}
		
		return simpleMatrix;
	}
}
